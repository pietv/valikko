import React, { Component } from 'react';
import {Modal,Button,Row,Col,Form} from 'react-bootstrap';
import Home from './Home';

export class AddRegistration extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleSubmit(event){
        event.preventDefault();
        fetch("http://localhost:8000/Person", {
            method:'POST',
            headers: {
                'Content-Type':'application/x-www-form-urlencoded'
            },
            body: new URLSearchParams({
                'first': event.target.RegFirst.value,
                'last': event.target.RegLast.value,
                'age': event.target.RegAge.value
              })
        })
        .then(res=> res.json())
        .then((result)=>
        {
            window.location.reload();
        },
        (error)=>{
            alert("failed");
        })
    }

    render() {
        return (
            <div className="container">
            <Modal
              {...this.props}
              size="lg"
              aria-labelledby="contained-modal-title-vcenter"
              centered
            >
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                  Add a registration
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                
                    <Row>
                        <Col sm={6}>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group controlId="RegFirst">
                                <Form.Label>First name</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="RegFirst"
                                    required
                                    placeholder="Hessu"
                                />
                                </Form.Group>

                                <Form.Group controlId="RegLast">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="RegLast"
                                    required
                                    placeholder="Hobo"
                                />
                                </Form.Group>

                                <Form.Group controlId="RegAge">
                                <Form.Label>Age</Form.Label>
                                <Form.Control
                                    type="number"
                                    name="RegAge"
                                    required
                                    placeholder="42"
                                />
                                </Form.Group>

                                <Form.Group>
                                    <Button variant="primary" type="submit" onClick={this.props.onHide}>Add Registration</Button>
                                </Form.Group>
                            </Form>

                        </Col>
                    </Row>
                
              </Modal.Body>
              <Modal.Footer>
                <Button variant="outline-primary" onClick={this.props.onHide}>Close</Button>
              </Modal.Footer>
            </Modal>
            </div>
          );
    }
}

export default AddRegistration;