import React, { Component } from 'react';
import {Modal,Button,Row,Col,Form} from 'react-bootstrap';

export class DelRegistration extends Component{
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event){
        event.preventDefault();
        fetch("http://localhost:8000/Person", {
            method:'DELETE',
            headers: {
                'Content-Type':'application/x-www-form-urlencoded'
            },
            body: new URLSearchParams({
                'id': event.target.RegID.value
              })
        })
        .then(res=> res.json())
        .then((result)=>
        {
            window.location.reload();
        },
        (error)=>{
            alert("failed");
        })

        
    }

    render() {
        return (
            <div className="container">
            <Modal
              {...this.props}
              size="lg"
              aria-labelledby="contained-modal-title-vcenter"
              centered
            >
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                  Delete this registration?
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                
                    <Row>
                        <Col sm={6}>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group controlId="RegFirst">
                                <Form.Label>First name</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="RegFirst"
                                    required
                                    disabled
                                    defaultValue = {this.props.regf}
                                />
                                </Form.Group>

                                <Form.Group controlId="RegLast">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="RegLast"
                                    required
                                    disabled
                                    defaultValue = {this.props.regl}
                                />
                                </Form.Group>

                                <Form.Group controlId="RegAge">
                                <Form.Label>Age</Form.Label>
                                <Form.Control
                                    type="number"
                                    name="RegAge"
                                    required
                                    disabled
                                    defaultValue = {this.props.rega}
                                />
                                </Form.Group>

                                <Form.Group controlId="RegID">
                                <Form.Label>ID</Form.Label>
                                <Form.Control
                                    type="number"
                                    name="RegID"
                                    required
                                    disabled
                                    defaultValue = {this.props.regi}
                                />
                                </Form.Group>

                                <Form.Group>
                                <Button variant="danger" type="submit" onClick={this.props.onHide}>Delete</Button>
                                </Form.Group>

                            </Form>

                        </Col>
                    </Row>
                
              </Modal.Body>
              <Modal.Footer>
                
                <Button variant="outline-primary" onClick={this.props.onHide}>Cancel</Button>
              </Modal.Footer>
            </Modal>
            </div>
        );
    }
}

export default DelRegistration;