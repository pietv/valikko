import React, { Component } from 'react';
import {Table} from 'react-bootstrap';
import {Button, ButtonToolbar} from 'react-bootstrap';
import {AddRegistration} from './AddRegistration';
import {EditRegistration} from './EditRegistration';
import {DelRegistration} from './DelRegistration';



export class Home extends Component {
    constructor(props) {
        super(props);
        this.state={regs : [], addModalShow : false, editModalShow : false, delModalShow : false}
    }

    componentWillMount() {
        this.refreshList();
    }

    refreshList() {
        fetch('http://localhost:8000/People')
        .then(res => res.json())
        .then((data) => {
            this.setState({regs:data["data"]});
        })
        .catch(console.log);
    }

 //   componentDidUpdate() {
 //       this.refreshList();
 //   }

    render() {
        const {regs, regf, regl, rega, regi} = this.state;
        let addModalClose =() => this.setState({addModalShow:false});
        let editModalClose =() => this.setState({editModalShow:false});
        let delModalClose=() => this.setState({delModalShow:false});
        
        return (
            <div className="container-fluid">
                <h1 align="center">Registrations</h1>
                <br/>
                
                <ButtonToolbar>
                    <Button
                    variant="primary"
                    size="lg" 
                    block
                    onClick={()=> this.setState({addModalShow: true})}
                    
                    >
                    Add Registration
                    </Button>
                </ButtonToolbar>
                <br/>
                <br/>

                <AddRegistration
                show={this.state.addModalShow}
                onHide={addModalClose}
                
                />
                
                
                <Table hover="true">
                    <thead>
                        <tr>
                            <th width="10%">Id</th>
                            <th width="35%">First</th>
                            <th width="35%">Last</th>
                            <th width="10%">Age</th>
                            <th width="10%">Options</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        {regs.map(reg=>  
                            <tr>
                                <td width="10%">{reg.id}</td>
                                <td width="35%">{reg.first}</td>
                                <td width="35%">{reg.last}</td>
                                <td width="10%">{reg.age}</td>
                                <td width="3%">
                                    
                                    <ButtonToolbar>
                                        <Button
                                        variant="outline-info"
                                        size="sm"
                                        className="mr-2"
                                        onClick={()=> this.setState({editModalShow:true, regi:reg.id, regf:reg.first, regl:reg.last, rega:reg.age})}
                                        >Edit</Button>

                                        <EditRegistration
                                            show={this.state.editModalShow}
                                            onHide={editModalClose}
                                            regi={regi}
                                            regf={regf}
                                            regl={regl}
                                            rega={rega}
                                        />
                                    </ButtonToolbar>
                                    </td>
                                    <td width="7%">
                                        <ButtonToolbar>
                                    <Button variant="outline-danger" 
                                    size="sm"
                                    className="mr-2"
                                    onClick={()=> this.setState({delModalShow:true, regi:reg.id, regf:reg.first, regl:reg.last, rega:reg.age})}>Delete</Button>
                                    <DelRegistration
                                            show={this.state.delModalShow}
                                            onHide={delModalClose}
                                            regi={regi}
                                            regf={regf}
                                            regl={regl}
                                            rega={rega}
                                        />
                                    </ButtonToolbar>
                                    </td>
                                    
                                    
                            </tr>
                        )}
                        
                        
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Home;