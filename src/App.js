import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import {BrowserRouter, Route, Link, Switch} from 'react-router-dom';
import AddRegistration from './components/AddRegistration';
import EditRegistration from './components/EditRegistration';
import Home from './components/Home';
import DelRegistration from './components/DelRegistration';
import Navigation from './components/Navigation';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Navigation/>
        <Switch>
          <Route path="/" component={Home} exact />
        </Switch>
      </BrowserRouter>
    );
  }

}

export default App;
